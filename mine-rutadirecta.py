from gevent import monkey

monkey.patch_all()

import requests
from pyquery import PyQuery as pq
import polyline
from pprint import pprint
import json
import gevent

def get_route(rid):
    res = requests.post('http://xalapa.rutadirecta.com/api/get/route', data={
        'id': rid,
    })
    data = res.json()[0]

    line = list(map(
        lambda x:[x[1], x[0]],
        polyline.decode(data['trayecto'].replace('\\\\', '\\'))
    ))

    del data['trayecto']

    with open(f'./archive/xalapa/{rid}.geojson', 'w') as linefile:
        json.dump({
            "type": "FeatureCollection",
            "features": [{
                "type": "Feature",
                "properties": data,
                "geometry": {
                    "type": "LineString",
                    "coordinates": line,
                }
            }]
        }, linefile)

def main():
    res = requests.get('http://xalapa.rutadirecta.com/')

    d = pq(res.text)

    gevent.joinall([
        gevent.spawn(get_route, tag.attrib['data-rid']) for tag in d('a.ruta')
    ])

if __name__ == '__main__':
    main()
