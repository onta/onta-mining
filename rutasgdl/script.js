// From https://pastebin.com/aKLNxv6Y
var local = false;
var map;
var myLoc;
var markersArray = [];
var routesArray = [];
var docroot = local ? 'rutasgdl/wp-content/themes/bueno/' : '/wp-content/themes/bueno/';
var loadingImgPath = docroot + 'images/loading.gif';
var loadingImg = new Image(16, 16);
loadingImg.src = loadingImgPath;
var endPointURL = local ? 'http://localhost/rutasgdl/mapas/' : 'http://' + window.location.hostname + '/mapas';
var userDefinedLoc = false;
var userMarker;
var infowindow;
var geocoder;
var la;
var lo;
var zo;
var ra;
var setArrows;
var mD = "unknown";
var isM = false;

function jaecLog(msg) {
    try {
        console.log(msg);
    } catch (e) {}
}

function var_dump(obj) {
    if (typeof obj == "object") {
        return "Type: " + typeof (obj) + ((obj.constructor) ? "\nConstructor: " + obj.constructor : "") + "\nValue: " + obj;
    } else {
        return "Type: " + typeof (obj) + "\nValue: " + obj;
    }
}

function isMobile() {
    if (mD == "unknown") {
        var deviceIphone = "iphone";
        var deviceIpod = "ipod";
        var deviceIpad = "ipad";
        var deviceAndroid = "android";
        var nstring = navigator.userAgent.toLowerCase();
        if ((nstring.search(deviceIphone) > -1) || (nstring.search(deviceIpod) > -1) || (nstring.search(deviceIpad) > -1) || (nstring.search(deviceAndroid) > -1)) {
            isM = true;
            if ((nstring.search(deviceIphone) > -1)) {
                mD = "iphone";
            } else if ((nstring.search(deviceIpod) > -1)) {
                mD = "ipod";
            } else if ((nstring.search(deviceIpad) > -1)) {
                mD = "ipad";
            } else if ((nstring.search(deviceAndroid) > -1)) {
                mD = "android";
            }
            return true;
        } else {
            isM = false;
            return false;
        }
    } else {
        return isM;
    }
}

function centerAtUserPosition() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var lat = position.coords.latitude;
            var lon = position.coords.longitude;
            var center = new google.maps.LatLng(lat, lon);
            map.setCenter(center);
            map.setZoom(18);
        }, function (error) {
            switch (error.code) {
            case error.TIMEOUT:
                break;
            case error.POSITION_UNAVAILABLE:
                break;
            case error.PERMISSION_DENIED:
                break;
            case error.UNKNOWN_ERROR:
                break;
            }
        });
    } else {}
}

function showAddrInMap(address) {
    address += ", Guadalajara, JAL, Mexico";
    geocoder.geocode({
        address: address
    }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            map.setZoom(16);
            getManualLocation();
        } else {
            try {
                console.log(status);
            } catch (e) {}
            alert("DirecciÃ³n no encontrada.");
        }
    });
}

function getManualLocation() {
    clearNearRoutesList();
    deleteOverlays();
    deleteRoutes();
    myLoc = map.getCenter();
    userMarker = insertDraggableMarker(myLoc, "Te encuentras aquÃ­", map);
    var contentString = '<div id="infowindowcontent">' + 'ArrÃ¡strame por el mapa, luego da clic en el botÃ³n "<a href="#!" onclick="s();">Buscar</a>"' + '</div>';
    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });
    google.maps.event.addListener(userMarker, 'click', function () {
        infowindow.open(map, userMarker);
    });
    infowindow.open(map, userMarker);
    userDefinedLoc = true;
}

function setManualLocation(lat, lon) {
    clearNearRoutesList();
    deleteOverlays();
    deleteRoutes();
    myLoc = new google.maps.LatLng(lat, lon);
    map.setCenter(myLoc);
    userMarker = insertDraggableMarker(myLoc, "Te encuentras aquÃ­", map);
    userDefinedLoc = true;
}

function setMyLocation(lat, lon) {
    var loc = new google.maps.LatLng(lat, lon);
    myLoc = loc;
    zoomToMyLoc(15);
    hideLoading();
}

function getLocation() {
    if (navigator.geolocation) {
        showLoading();
        navigator.geolocation.getCurrentPosition(function (position) {
            var lat = position.coords.latitude;
            var lon = position.coords.longitude;
            jQuery("#msg")
                .html("Te encuentras en latitud: '" + lat + "' y longitud: '" + lon + "'");
            setMyLocation(lat, lon);
        }, function (error) {
            switch (error.code) {
            case error.TIMEOUT:
                alert('Tiempo de espera excedido.');
                break;
            case error.POSITION_UNAVAILABLE:
                alert('PosiciÃ³n no encontrada.');
                break;
            case error.PERMISSION_DENIED:
                alert('Permiso denegado.');
                break;
            case error.UNKNOWN_ERROR:
                alert('Error desconocido.');
                break;
            }
            jQuery("#msg")
                .html("OcurriÃ³ un error: '" + error.message + "', cÃ³digo: '" + error.code + "'.<br> Â¿Geode y Firefox estÃ¡n instalados? ObtÃ©n Geocode <a href='https://people.mozilla.com/%7Edolske/dist/geode/geode-latest.xpi'>aquÃ­</a>.");
            hideLoading();
        });
    } else {
        jQuery("#msg")
            .html("Geode no estÃ¡ instalado. Obtenlo <a href='https://people.mozilla.com/%7Edolske/dist/geode/geode-latest.xpi'>aquÃ­</a>.");
    }
}

function requestRouteToRoute(lat, lon, id, o) {
    jQuery.ajax({
        url: endPointURL + '/getRouteToRouteJSON.php?lat=' + lat + '&lon=' + lon + '&rid=' + id + '&o=' + o,
        success: function (data) {
            var simpleroute = data;
            calcWalkRoute(new google.maps.LatLng(simpleroute.startPoint.lat, simpleroute.startPoint.lon), new google.maps.LatLng(simpleroute.endPoint.lat, simpleroute.endPoint.lon));
        },
        complete: function () {
            hideLoading();
        }
    });
}

function getRouteToRoute(id, o) {
    if (navigator.geolocation) {
        showLoading();
        navigator.geolocation.getCurrentPosition(function (position) {
            var lat = position.coords.latitude;
            var lon = position.coords.longitude;
            var loc = new google.maps.LatLng(lat, lon);
            myLoc = loc;
            requestRouteToRoute(lat, lon, id, o);
        }, function (error) {
            switch (error.code) {
            case error.TIMEOUT:
                alert('Tiempo de espera excedido.');
                break;
            case error.POSITION_UNAVAILABLE:
                alert('PosiciÃ³n no encontrada.');
                break;
            case error.PERMISSION_DENIED:
                alert('Permiso denegado.');
                break;
            case error.UNKNOWN_ERROR:
                alert('Error desconocido.');
                break;
            }
            hideLoading();
        });
    } else {}
}

function requestDrivingRouteToRoute(lat, lon, id, o) {
    jQuery.ajax({
        url: endPointURL + '/getRouteToRouteJSON.php?lat=' + lat + '&lon=' + lon + '&rid=' + id + '&o=' + o,
        success: function (data) {
            var simpleroute = data;
            calcDrivingRoute(new google.maps.LatLng(simpleroute.startPoint.lat, simpleroute.startPoint.lon), new google.maps.LatLng(simpleroute.endPoint.lat, simpleroute.endPoint.lon));
        },
        complete: function () {
            hideLoading();
        }
    });
}

function getDrivingRouteToRoute(id, o) {
    if (navigator.geolocation) {
        showLoading();
        navigator.geolocation.getCurrentPosition(function (position) {
            var lat = position.coords.latitude;
            var lon = position.coords.longitude;
            var loc = new google.maps.LatLng(lat, lon);
            myLoc = loc;
            requestDrivingRouteToRoute(lat, lon, id, o);
        }, function (error) {
            switch (error.code) {
            case error.TIMEOUT:
                alert('Tiempo de espera excedido.');
                break;
            case error.POSITION_UNAVAILABLE:
                alert('PosiciÃ³n no encontrada.');
                break;
            case error.PERMISSION_DENIED:
                alert('Permiso denegado.');
                break;
            case error.UNKNOWN_ERROR:
                alert('Error desconocido.');
                break;
            }
            hideLoading();
        });
    } else {}
}

function createDraw(route, map, colors, o) {
    return function () {
        if (o !== undefined) {
            if (o == 1) {
                drawERoute(route.encodedPath, route.id, route.name, map, colors);
            } else if (o == 2) {
                drawERoute(route.encodedPath2, route.id, route.name, map, colors);
            }
        } else {
            drawERoute(route.encodedPath, route.id, route.name, map, colors);
            drawERoute(route.encodedPath2, route.id, route.name, map, colors);
        }
    }
}

function clearNearRoutesList() {
    jQuery('#nearRoutesList')
        .html('<ul></ul>');
}

function clearRoutesList() {
    jQuery('#routesList')
        .html('<ul></ul>');
}

function requestNearRoutes(lat, lon, ratio, nightly) {
    var endURL = '/getNearRoutesJSON.php';
    if (nightly === true) {
        endURL = '/getNearNightlyRoutesJSON.php';
    }
    jQuery.ajax({
        url: endPointURL + endURL + '?lat=' + lat + '&lon=' + lon + '&ratio=' + ratio,
        success: function (data) {
            deleteOverlays();
            deleteRoutes();
            if (data.routecount == "0") {
                alert("No se encontraron rutas cercanas a esa distancia seleccionada (" + ratio + " metros) \nPrueba de nuevo aumentando la distancia.");
            }
            var routes = data.routes;
            var colors = generateColors(routes.length + 1);
            clearNearRoutesList();
            for (i = 0; i < routes.length; i++) {
                var route = routes[i];
                jQuery('#nearRoutesList > ul')
                    .append('<li><a target="_blank" style="color: white" href="' + route.url + '">' + route.name + '</a></li>');
                var _fd = createDraw(route, map, colors[i]);
                setTimeout(_fd, 100 + i);
            }
            var zoom = 13;
            if (ratio <= 20) {
                zoom = 17;
            }
            if (ratio > 20 && ratio <= 200) {
                zoom = 16;
            }
            if (ratio > 200 && ratio <= 900) {
                zoom = 15;
            }
            if (ratio > 900) {
                zoom = 14;
            }
            if (!userDefinedLoc) {
                setTimeout(function () {
                    zoomToMyLoc(zoom)
                }, 101 + i);
            } else {
                setTimeout(function () {
                    map.setCenter(myLoc);
                    insertMarker(myLoc, "Te encuentras aquÃ­", map);
                }, 101 + i);
            }
        },
        complete: function () {
            hideLoading();
            ra = ratio;
            la = lat;
            lo = lon;
            zo = map.getZoom();
            jaecLog("embed vars setted.")
            jQuery("#share-embed-size-default")
                .click();
            try {
                var newhash = '!ratio=' + ratio + '&lat=' + lat + '&lon=' + lon + '&z=' + map.getZoom();
                window.location.hash = newhash;
            } catch (e) {}
        }
    });
}

function searchNearRoutes(ratio, nightly) {
    showLoading();
    if (!userDefinedLoc) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var lat = position.coords.latitude;
                var lon = position.coords.longitude;
                var loc = new google.maps.LatLng(lat, lon);
                myLoc = loc;
                requestNearRoutes(lat, lon, ratio, nightly);
            }, function (error) {
                switch (error.code) {
                case error.TIMEOUT:
                    alert('Tiempo de espera excedido.');
                    break;
                case error.POSITION_UNAVAILABLE:
                    alert('PosiciÃ³n no encontrada.');
                    break;
                case error.PERMISSION_DENIED:
                    alert('Permiso denegado.');
                    break;
                case error.UNKNOWN_ERROR:
                    alert('Error desconocido.');
                    break;
                }
            });
        } else {}
    } else {
        myLoc = userMarker.getPosition();
        requestNearRoutes(myLoc.lat(), myLoc.lng(), ratio, nightly);
    }
}

function requestRoute(id, o) {
    showLoading();
    jQuery.ajax({
        url: endPointURL + '/getRouteJSON.php?&rid=' + id,
        success: function (data) {
            var route = data
            var colors = generateColors(1);
            jQuery('#routesList > ul')
                .append('<li><a target="_blank" style="color: white" href="' + route.url + '">' + route.name + '</a></li>');
            var _fd = createDraw(route, map, colors[0], o);
            setTimeout(_fd, 100);
        },
        complete: function () {
            hideLoading();
        }
    });
}

function showLoading() {
    jQuery('#loading')
        .html('<img style="border: 0" src="' + loadingImgPath + '" border="0" id="loading-gif" />');
}

function hideLoading() {
    jQuery('#loading')
        .html('');
}

function zoomToMyLoc(zoom) {
    insertMarker(myLoc, "Te encuentras aquÃ­", map);
    map.setZoom(zoom);
    map.setCenter(myLoc);
}

function geodistance(start, end) {
    var R = 6371;
    var lat1 = start.lat();
    var lon1 = start.lng();
    var lat2 = end.lat();
    var lon2 = end.lng();
    var dLat = (lat2 - lat1) * Math.PI / 180;
    var dLon = (lon2 - lon1) * Math.PI / 180;
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
}

function drawPoints(map, points, color, fitBounds, routeInfo) {
    var routePath = new google.maps.Polyline({
        path: points,
        strokeColor: color,
        strokeOpacity: 0.7,
        strokeWeight: 8
    });
    if (fitBounds) {
        fit(map, points[0], points[points.length - 1], 5);
    }
    setArrows.load(points, "midline");
    routePath.setMap(map);
    routesArray.push(routePath);
    google.maps.event.addListener(routePath, 'click', function () {
        alert(routeInfo);
    });
}

function fit(map, startLatLng, endLatLng, threshold) {
    var rectangule;
    var north;
    var south;
    var east;
    var west;
    var ne;
    var sw;
    if (startLatLng.lat() - endLatLng.lat() > 0) {
        north = startLatLng.lat();
        south = endLatLng.lat();
    } else {
        north = endLatLng.lat()
        south = startLatLng.lat();
    }
    if (startLatLng.lng() - endLatLng.lng() > 0) {
        east = startLatLng.lng();
        west = endLatLng.lng();
    } else {
        east = endLatLng.lng();
        west = startLatLng.lng();
    }
    ne = new google.maps.LatLng(north, east);
    sw = new google.maps.LatLng(south, west);
    if (geodistance(sw, ne) < threshold) {
        rectangule = convertToArea(sw.lat(), sw.lng(), ne.lat(), ne.lng(), threshold);
    } else {
        rectangule = new google.maps.LatLngBounds(sw, ne);
    }
    map.fitBounds(rectangule);
    map.setCenter(rectangule.getCenter());
}

function drawWalkingRoute(map, route, color) {
    if (color === undefined) {
        color = "#9900FF";
    }
    var routePath = new google.maps.Polyline({
        path: route,
        strokeColor: color,
        strokeOpacity: 0.7,
        strokeWeight: 4
    });
    routePath.setMap(map);
    insertMarker(route[0], "Inicio de caminata", map);
    insertMarker(route[route.length - 1], "Fin de caminata", map);
}

function drawDrivingRoute(map, route, color) {
    if (color === undefined) {
        color = "#FF0099";
    }
    var routePath = new google.maps.Polyline({
        path: route,
        strokeColor: color,
        strokeOpacity: 0.7,
        strokeWeight: 4
    });
    routePath.setMap(map);
    insertMarker(route[0], "Inicio del recorrido", map);
    insertMarker(route[route.length - 1], "Fin del recorrido", map);
}

function drawERoute(instring, routeID, routeName, map, color, fitBounds) {
    if (color === undefined) {
        color = "#663399";
    }
    if (fitBounds === undefined) {
        fitBounds = false;
    }
    instring = decode(instring);
    instring = instring.replace(/\\\\/g, "\\");
    drawRoute(instring, routeID, routeName, map, color, fitBounds);
}

function clearOverlays() {
    if (markersArray) {
        for (i in markersArray) {
            markersArray[i].setMap(null);
        }
    }
    if (setArrows !== undefined) {
        setArrows.clearMarkers();
    }
}

function deleteOverlays() {
    if (markersArray) {
        for (i in markersArray) {
            markersArray[i].setMap(null);
        }
        markersArray.length = 0;
    }
    if (setArrows !== undefined) {
        setArrows.deleteMarkers();
    }
}

function clearRoutes() {
    if (routesArray) {
        for (i in routesArray) {
            routesArray[i].setMap(null);
        }
    }
}

function deleteRoutes() {
    if (routesArray) {
        for (i in routesArray) {
            routesArray[i].setMap(null);
        }
        routesArray.length = 0;
    }
    jQuery("#testtextarea")
        .val('');
}

function insertMarker(position, title, map, image, marker) {
    if (marker === undefined) {
        if (image != undefined) {
            var marker = new google.maps.Marker({
                flat: true,
                map: map,
                position: position,
                title: title,
                icon: image
            });
        } else {
            var marker = new google.maps.Marker({
                flat: true,
                map: map,
                position: position,
                title: title
            });
        }
    }
    markersArray.push(marker);
    return marker;
}

function insertDraggableMarker(position, title, map, marker) {
    if (marker === undefined) {
        var marker = new google.maps.Marker({
            draggable: true,
            flat: true,
            map: map,
            position: position,
            title: title
        });
    }
    markersArray.push(marker);
    return marker;
}

function drawRoute(instring, routeID, routeName, map, color, fitBounds) {
    if (color === undefined) {
        color = "#663399";
    }
    if (fitBounds === undefined) {
        fitBounds = false;
    }
    var routeCoordinates = Array();
    var points = decodePolyline(instring);
    for (i = 0; i < points.length; i++) {
        var p = new google.maps.LatLng(points[i][0], points[i][1]);
        routeCoordinates.push(p);
    }
    drawPoints(map, routeCoordinates, color, fitBounds, "Ruta " + routeName);
    var startMarker = insertMarker(routeCoordinates[0], "Inicio de la ruta", map, "http://rutasgdl.com" + docroot + "images/mm_20_green.png");
    var endMarker = insertMarker(routeCoordinates[routeCoordinates.length - 1], "Fin de la ruta", map, "http://rutasgdl.com" + docroot + "images/mm_20_red.png");
    var startIWindow = new google.maps.InfoWindow({
        content: '<p>Inicio de la ruta ' + routeName + '</p>'
    });
    var endIWindow = new google.maps.InfoWindow({
        content: '<p>Fin de la ruta ' + routeName + '</p>'
    });
    google.maps.event.addListener(startMarker, "click", function () {
        startIWindow.open(map, startMarker);
    });
    google.maps.event.addListener(endMarker, "click", function () {
        endIWindow.open(map, endMarker);
    });
}

function generateColors(numberOfColors) {
    var colors = Array();
    var db4 = [
        ["#222222", "#99ffcc", "#9999ff", "#ff99cc"],
        ["#222222", "#33ff99", "#3333ff", "#ff3399"],
        ["#222222", "#99ff99", "#99ccff", "#ff99ff"],
        ["#222222", "#33ff66", "#3366ff", "#ff33cc"],
        ["#ff99ff", "#222222", "#99ff99", "#99ccff"],
        ["#222222", "#ccff99", "#99ffff", "#cc99ff"],
        ["#222222", "#33ff33", "#3399ff", "#ff33ff"],
        ["#ff6699", "#e5ff66", "#66ffcc", "#7f66ff"],
        ["#ff6666", "#b2ff66", "#66ffff", "#b266ff"],
        ["#ff33ff", "#222222", "#33ff33", "#33ff33"],
        ["#ff3300", "#4cff00", "#00ccff", "#b200ff"],
        ["#99ff33", "#33ffff", "#9933ff", "#ff3333"],
        ["#99cc00", "#00cc99", "#3300cc", "#cc0033"],
        ["#993399", "#996632", "#329932", "#326699"],
        ["#6633cc", "#cc334c", "#99cc33", "#33ccb2"],
        ["#6666ff", "#ff66b2", "#ffff66", "#66ffb2"],
        ["#009933", "#001999", "#990066", "#997f00"],
        ["#006699", "#7f0099", "#993300", "#199900"],
        ["#0066cc", "#cc00cc", "#cc6600", "#00cc00"],
        ["#cc6633", "#4ccc33", "#3399cc", "#b233cc"],
        ["#333399", "#993266", "#999932", "#329966"],
        ["#660033", "#666600", "#006633", "#000066"],
        ["#990099", "#994c00", "#009900", "#004c99"],
        ["#0033ff", "#ff00b2", "#ffcc00", "#00ff4c"]
    ];
    for (i = 0; i < numberOfColors; i += 4) {
        var j = Math.floor(Math.random() * db4.length + 1) - 1;
        colors.push(db4[j][0]);
        colors.push(db4[j][1]);
        colors.push(db4[j][2]);
        colors.push(db4[j][3]);
    }
    return colors;
}

function calcWalkRoute(start, end) {
    showLoading();
    var directionsService = new google.maps.DirectionsService();
    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.DirectionsTravelMode.WALKING
    };
    directionsService.route(request, function (result, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            route = result.routes[0].overview_path;
            drawWalkingRoute(map, route);
            fit(map, route[0], route[route.length - 1], 2);
        }
        hideLoading();
    });
}

function calcDrivingRoute(start, end) {
    showLoading();
    var directionsService = new google.maps.DirectionsService();
    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    directionsService.route(request, function (result, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            route = result.routes[0].overview_path;
            drawDrivingRoute(map, route);
            fit(map, route[0], route[route.length - 1], 2);
        }
        hideLoading();
    });
}

function degreesToRadians(degrees) {
    var pi = Math.PI;
    var radians = degrees * (pi / 180);
    return radians;
}

function convertToArea(slat, slon, elat, elon, threshold) {
    var lon1 = slon - threshold / Math.abs(Math.cos(degreesToRadians(slat)) * 69);
    var lon2 = elon + threshold / Math.abs(Math.cos(degreesToRadians(elat)) * 69);
    var lat1 = slat - (threshold / 69);
    var lat2 = elat + (threshold / 69);
    var rectangule = new google.maps.LatLngBounds(new google.maps.LatLng(lat1, lon1), new google.maps.LatLng(lat2, lon2));
    return rectangule;
}

function ArrowHandler(m) {
    this.setMap(m);
    this.arrowheads = [];
    this.markersArray = [];
}
ArrowHandler.prototype = new google.maps.OverlayView();
ArrowHandler.prototype.draw = function () {
    if (this.arrowheads.length > 0) {
        for (var i = 0, m; m = this.arrowheads[i]; i++) {
            m.setOptions({
                position: this.usePixelOffset(m.p1, m.p2)
            });
        }
    }
};
ArrowHandler.prototype.usePixelOffset = function (p1, p2) {
    var proj = this.getProjection();
    var g = google.maps;
    var dist = 12;
    var pix1 = proj.fromLatLngToContainerPixel(p1);
    var pix2 = proj.fromLatLngToContainerPixel(p2);
    var vector = new g.Point(pix2.x - pix1.x, pix2.y - pix1.y);
    var length = Math.sqrt(vector.x * vector.x + vector.y * vector.y);
    var normal = new g.Point(vector.x / length, vector.y / length);
    var offset = new g.Point(pix2.x - dist * normal.x, pix2.y - dist * normal.y);
    return proj.fromContainerPixelToLatLng(offset);
};
ArrowHandler.prototype.addIcon = function (file) {
    var g = google.maps;
    var icon = new g.MarkerImage("http://www.google.com/mapfiles/" + file, new g.Size(24, 24), null, new g.Point(12, 12));
    return icon;
};
ArrowHandler.prototype.create = function (p1, p2, mode) {
    var markerpos;
    var g = google.maps;
    if (mode == "onset") markerpos = p1;
    else if (mode == "head") markerpos = this.usePixelOffset(p1, p2);
    else if (mode == "midline") markerpos = g.geometry.spherical.interpolate(p1, p2, .5);
    var dir = g.geometry.spherical.computeHeading(p1, p2)
        .toFixed(1);
    dir = Math.round(dir / 3) * 3;
    if (dir < 0) dir += 240;
    if (dir > 117) dir -= 120;
    var icon = this.addIcon("dir_" + dir + ".png");
    var marker = new g.Marker({
        position: markerpos,
        map: map,
        icon: icon,
        clickable: false
    });
    this.markersArray.push(marker);
    if (mode == "head") {
        marker.p1 = p1;
        marker.p2 = p2;
        this.arrowheads.push(marker);
    }
};
ArrowHandler.prototype.clearMarkers = function () {
    if (this.markersArray) {
        for (i in this.markersArray) {
            this.markersArray[i].setMap(null);
        }
    }
}
ArrowHandler.prototype.deleteMarkers = function () {
    if (this.markersArray) {
        for (i in this.markersArray) {
            this.markersArray[i].setMap(null);
        }
        this.markersArray.length = 0;
    }
}
ArrowHandler.prototype.isPrime = function (n) {
    if (isNaN(n) || !isFinite(n) || n % 1 || n < 2) return false;
    var m = Math.sqrt(n);
    for (var i = 2; i <= m; i++) if (n % i == 0) return false;
    return true;
}
ArrowHandler.prototype.load = function (points, mode) {
    for (var i = 0; i < points.length - 1; i++) {
        if (i % 13 != 0) continue;
        var p1 = points[i],
            p2 = points[i + 1];
        this.create(p1, p2, mode);
    }
};

function progressBar(progress) {
    var html = "<span style='display: inline;'><table style='width: 750px; border: 1px solid black;' cellspacing='1' cellpadding='0' border='0'>";
    html += "<tr>";
    for (var ix = 0; ix < 100; ix = ix + 2) {
        if (ix < progress) html += "<td class='green' nowrap='nowrap'>&nbsp;</td>";
        else html += "<td class='red' nowrap='nowrap'>&nbsp;</td>";
    }
    html += "</tr>";
    html += "</table></span>";
    return html;
}

function getParameterByName(name) {
    var lochash = window.location.hash.substr(1);
    value = lochash.substr(lochash.indexOf(name + '='))
        .split('&')[0].split('=')[1];
    if (value != undefined) {
        value = decodeURIComponent(value);
    }
    return value;
}

function insertHashParam(key, value) {
    key = escape(key);
    value = escape(value);
    var hash = window.location.hash.substr(1);
    var newhash = hash + "&" + key + "=" + encodeURIComponent(value);
    window.location.hash = newhash;
}
