const puppeteer = require('puppeteer');
const fs = require('mz/fs');

async function get_single_route(browser, url) {
  const identifier = url.split('/')[4];
  const filename = `archive/${identifier}.geojson`;

  try {
    await fs.access(filename);

    return console.log(`Already had ${filename}, skipping`);
  } catch (err) { }

  console.log('visiting', url);

  const route_page = await browser.newPage();
  const re = /(drawERoute\("([^"]*)\")/;

  await route_page.goto(url, {
    timeout: 0,
  });

  const content = await route_page.content();
  const encodedpath = content.match(re)[2];

  const data = await route_page.evaluate((instring) => {
    instring = decode(instring);
    instring = instring.replace(/\\\\/g, "\\");

    return {
      positionArray: decodePolyline(instring),
      name: $('.title a').text(),
    };
  }, encodedpath);

  geojson = JSON.stringify({
    "type": "FeatureCollection",
    "features": [
      {
        "type": "Feature",
        "properties": {
          identifier: identifier,
          name: data.name,
        },
        "geometry": {
          "type": "LineString",
          "coordinates": data.positionArray.map((i) => [i[1], i[0]]),
        }
      }
    ]
  }, null, 2);

  await fs.writeFile(filename, geojson);
  await route_page.close();
}

(async () => {
  if (process.argv.length < 4) {
    return console.log(`usage: node mine.js <lower> <upper>

lower: start from this index in the route list
upper: the first index that is not downloaded`);
  }

  const browser = await puppeteer.launch({
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
    headless: false,
  });

  let urls = [];

  try {
    let contents = await fs.readFile('archive/list.json');

    urls = JSON.parse(contents);
  } catch (err) {
    const page = (await browser.pages())[0];
    await page.goto('https://rutasgdl.com/posts/listado-de-rutas');

    urls = await page.evaluate(() => {
      let l = [];
      jQuery('ul.list li a').map((i, a) => l.push(a.attributes.href.nodeValue));

      return l;
    });

    console.log('Found', urls.length, 'routes');

    fs.writeFile('archive/list.json', JSON.stringify(urls, null, 2));
  }

  const start = Math.max(parseInt(process.argv[2]), 0);
  const end = Math.min(parseInt(process.argv[3]), urls.length);

  let promises = [];

  for (let i=start; i<end; i++) {
    promises.push(get_single_route(browser, urls[i]));
    promises.push(get_single_route(browser, urls[i]+'?o=2'));
  }

  await Promise.all(promises);

  // this happens on the route page
  await browser.close();
})();
